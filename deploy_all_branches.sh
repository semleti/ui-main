#!/bin/sh
#parameters:
#1: OUT_DIR
#2...n: branches to deploy besides master

#GOAL: deploy all specified branches at once to the pages any time one of them changes

echo "deploy_all_branches parameters:"
echo "$@"

#get current branch to return to it after we're done
INITIAL_BRANCH=$(git rev-parse --abbrev-ref HEAD)

#make sure git knows all the things
git fetch

mkdir -p public

# Check out a branch, copy its build to the pages stuff inside the specified directory
deploy_to_page_directory()
{
    #$1: directory $2:branch
    mkdir -p $1

    echo "gonna check out $2"

    git stash

    git checkout $2
    
    echo "checkout $2"

    cp -r * $1
}

# It is important to start with master branch:
deploy_to_page_directory public master


#iterate over all specified branches and deploy them to a subdirectory
for BRANCH in $@; do
    echo "loop for $BRANCH"
    deploy_to_page_directory public/$BRANCH $BRANCH
done

#return to initial branch
git checkout $INITIAL_BRANCH
